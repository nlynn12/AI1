/*
 *      University of Southern Denmark
 *      Introduction to Artificial Intelligence
 *
 *      Project:        Sokoban Solver
 *      Module name:    map.hpp
 *
 *      Description:    Implements map container class
 *
 *      Author(s):      Juan Luis Hortelano Villanueva
 *                      Nicolai Anton Lynnerup
 *
 *      ------------------------------------------------------
 *      Change Log:
 *
 *      Date            ID              Change
 *      YYMMDD
 *      151118          NAL             Module Created
 *
 *      ------------------------------------------------------
 */

#ifndef _MAP_HPP
#define _MAP_HPP

/***************************** Include files *******************************/
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <unordered_map>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <stdint.h>
#include <functional>
#include <utility>
#include <boost/assign/list_of.hpp>
#include <queue>

#include "position.hpp"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

using namespace std;

class map{
public:
    enum symbol : char
    {
        WALL         = 'X',
        DIAMOND      = 'J',
        GOAL         = 'G',
        FREE         = '.',
        MAN          = 'M',
        MAN_GOAL     = 'm',
        DIAMOND_GOAL = 'j',
        OUTSIDE      = ' ',
        DEADLOCK     = 'D',
        MAN_DEADLOCK = 'd',
        UNKNOWN      = '-'
    };

    map() = default;
    map(string map_file_path);

    symbol get_symbol(Position &pos);
    Position get_man_position() const;
    vector<Position> get_diamond_positions() const;
    vector<Position> get_goal_positions() const;
    bool solved();

    void move_man(Position &new_pos);
    void push_diamond(Position &current_pos, Position &new_pos);
    uint8_t get_distance_goal(Position &current_pos);
    uint8_t get_distance_diamond(Position &current_pos);
    void calc_distance_diamonds();
    void print();
private:
    typedef vector<vector<uint8_t> > wavefront_t;
    typedef vector<Position> positions_t;

    static uint8_t width,
                   height,
                   num_of_diamonds;

    static vector<vector<symbol> > layout;
    static wavefront_t distance_diamonds;
    static wavefront_t distance_goals;

    Position man_position;
    vector<Position> diamond_positions;
    vector<Position>::iterator diamond_positions_itr;
    static vector<Position> goal_positions;

    symbol get_symbol_on_layout(Position &pos);
    void remove_man_and_diamonds();
    void add_man_and_diamonds();
    void set_symbol(Position &pos, symbol new_symbol);
    void build_wavefront(wavefront_t &wavefront, positions_t &from_positions);
    void init_wavefront(wavefront_t &wavefront);
    void clear_wavefront(wavefront_t &wavefront);

    //Functions used for parsing the sokoban map file
    void parse_map_file(string map_file_path);
    void get_map_info(ifstream &map_file);
    void build_layout(ifstream &map_file);
};


#endif //_MAP_HPP
