/*
 *      University of Southern Denmark
 *      Introduction to Artificial Intelligence
 *
 *      Project:        Sokoban Solver
 *      Module name:    solver.hpp
 *
 *      Description:    Implements the Sokoban Map
 *
 *      Author(s):      Juan Luis Hortelano Villanueva
 *                      Nicolai Anton Lynnerup
 *
 *      ------------------------------------------------------
 *      Change Log:
 *
 *      Date            ID              Change
 *      YYMMDD
 *      151109          NAL             Module Created
 *
 *      ------------------------------------------------------
 */

#ifndef _SOLVER_HPP
#define _SOLVER_HPP

/***************************** Include files *******************************/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <queue>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <time.h>

#include "position.hpp"
/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

using namespace std;

class Solver{
public:
    Solver(string path);
    void run();
private:
    // Map
    vector<vector<char>> map;
    int width, height, nJewels;

    vector<Position> listJewels;
    vector<Position> listGoals;
    Position mPos;

    void load_map(string path);
    void detect_deadlocks();

    // Nodes
    struct node{
        node(int heuristic) : heuristic(heuristic) {}
        int heuristic;
        int cost;

        /// ADD: vector of mPos and jPos for use in deque

        bool operator<(node rhs) const{
            return heuristic > rhs.heuristic;
        }
    }root, solution;

    // Solver
    // Maybe create a hash table??
    deque<Position> closed_list;
    // Min heap of nodes to always pop the node with smallest distance to goal
    priority_queue<node> open_list;

    enum MOVEDIR{
        LEFT  = 'L',
        UP    = 'U',
        RIGHT = 'R',
        DOWN  = 'D'
    };

    void add_node(node curr_node, MOVEDIR dir);
    bool validate_move(node curr_node, MOVEDIR dir);
    bool is_solved();

    //Heuristics (by Wavefront)
};


#endif // _SOLVER_HPP
