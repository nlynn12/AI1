/*
 *      University of Southern Denmark
 *      Introduction to Artificial Intelligence
 *
 *      Project:        Sokoban Solver
 *      Module name:    position.hpp
 *
 *      Description:    Implements "subclass" for position holding
 *
 *      Author(s):      Juan Luis Hortelano Villanueva
 *                      Nicolai Anton Lynnerup
 *
 *      ------------------------------------------------------
 *      Change Log:
 *
 *      Date            ID              Change
 *      YYMMDD
 *      151118          NAL             Module Created
 *
 *      ------------------------------------------------------
 */

#ifndef _POSITION_HPP
#define _POSITION_HPP

/***************************** Include files *******************************/
#include <iostream>
#include <tuple>
#include <math.h>

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/



class Position{
private:
    int x = 0;
    int y = 0;
public:
    Position() = default;
    Position(int x, int y);
    bool operator ==(const Position &rhs) const;
    bool operator !=(const Position &rhs) const;
    bool operator <(const Position &rhs) const;

    int get_x() const;
    int get_y() const;
    void set_x(int x);
    void set_y(int y);
    void set_xy(int x, int y);
    float get_distance(Position to_pos);
    void print();
};

#endif // _POSITION_HPP
