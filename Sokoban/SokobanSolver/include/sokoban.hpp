/*
 *      University of Southern Denmark
 *      Introduction to Artificial Intelligence
 *
 *      Project:        Sokoban Solver
 *      Module name:    sokoban.hpp
 *
 *      Description:    Implements the Sokoban Game
 *
 *      Author(s):      Juan Luis Hortelano Villanueva
 *                      Nicolai Anton Lynnerup
 *
 *      ------------------------------------------------------
 *      Change Log:
 *
 *      Date            ID              Change
 *      YYMMDD
 *      151118          NAL             Module Created
 *
 *      ------------------------------------------------------
 */

#ifndef _SOKOBAN_HPP
#define _SOKOBAN_HPP

/***************************** Include files *******************************/
#include <iostream>

#include "map.hpp"
#include "position.hpp"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

class Sokoban{
public:
    enum direction : char{
        NONE   = ' ',
        UP     = 'u',
        DOWN   = 'd',
        LEFT   = 'l',
        RIGHT  = 'r',
    };

    Sokoban(map &map_);
    void move_man(direction dir);
    char get_move_symbol();
private:
    enum result{
        INVALID,
        MOVE,
        PUSH
    };

    map &map_;
    direction man_move_direction;
    result    man_move_result;

    result validate_move(Position &move);
    bool   validate_push(Position &push);
    Position get_new_position(Position &from_pos, direction dir);
    void set_map(map &map_);
};

#endif // _SOKOBAN_HPP
