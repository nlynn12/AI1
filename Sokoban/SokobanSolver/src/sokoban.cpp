#include "sokoban.hpp"

using namespace std;

Sokoban::Sokoban(map &map_) : map_(map_){}

void Sokoban::move_man(direction dir){
    man_move_direction = dir;

    position man_position = map_.get_man_position();

    //Get the new position for the move of the man
    position new_pos = get_new_position(man_position, man_move_direction);
    //Validate the move
    man_move_result = validate_move(new_pos);

    //Take action according to the result of the move
    switch (man_move_result){
        case INVALID:
            man_move_direction = NONE;
            break;
        case MOVE:
            map_.move_man(new_pos);
            break;
        case PUSH:{
            position new_pos_diamond = get_new_position(new_pos, man_move_direction);
            if(validate_push(new_pos_diamond)){
                map_.push_diamond(new_pos, new_pos_diamond);
            }
            break;
        }
        default:
            break;
    }
}


bool Sokoban::validate_push(position &push){
    switch (map_.get_symbol(push)){
        case map::WALL:
        case map::OUTSIDE:
        case map::MAN:
        case map::MAN_GOAL:
        case map::DIAMOND:
        case map::DIAMOND_GOAL:
        case map::DEADLOCK:
        case map::MAN_DEADLOCK:
            return false;
            break;
        case map::GOAL:
        case map::FREE:
            return true;
            break;
        default:
            cerr << "Not a valid symbol." << endl;
            break;
    }
}


Sokoban::result Sokoban::validate_move(position &move){
    switch (map_.get_symbol(move)){
        case map::WALL:
        case map::OUTSIDE:
        case map::MAN:
        case map::MAN_GOAL:
        case map::MAN_DEADLOCK:
            return INVALID;
            break;
        case map::DIAMOND:
        case map::DIAMOND_GOAL:
            return PUSH;
            break;
        case map::DEADLOCK:
        case map::GOAL:
        case map::FREE:
            return MOVE;
            break;
        default:
            cerr << "Not a valid symbol." << endl;
            break;
    }
}


position Sokoban::get_new_position(position &from_pos, direction dir){
    position new_pos;

    switch (dir){
        case UP:
            new_pos.set_xy(from_pos.get_x(), from_pos.get_y() - 1);
            break;
        case DOWN:
            new_pos.set_xy(from_pos.get_x(), from_pos.get_y() + 1);
            break;
        case LEFT:
            new_pos.set_xy(from_pos.get_x() - 1, from_pos.get_y());
            break;
        case RIGHT:
            new_pos.set_xy(from_pos.get_x() + 1, from_pos.get_y());
            break;
        default:
            break;
    }
    return new_pos;
}


char Sokoban::get_move_symbol(){
    return man_move_result == PUSH ? toupper((char)man_move_direction) : man_move_direction ;
}


void Sokoban::set_map(map &map_){
    this->map_ = map_;
}
