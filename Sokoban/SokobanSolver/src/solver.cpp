#include "solver.hpp"
using namespace std;

Solver::Solver(string path){
    load_map(path);
    detect_deadlocks();
    // Perform wavefront from all goals (goals doesn't move so this map is the same for all nodes)
}

void Solver::load_map(string path){
    // Load map
    ifstream infile(path);
    string line;

    if(!infile.is_open()){
        cout << "Unable to open: \"" + path + "\"" << endl;
        exit(-1);
    } else {
        cout << "Map: \"" + path + "\" read "<< endl;

        // Load in Map info
        getline(infile, line);
        stringstream ss;
        ss << line;
        ss >> width >> height >> nJewels;

        // Load in map
        map.resize(this->height,vector<char>(this->width));
        for(int row = 0; row < height; row++){
            getline(infile, line);
            for(int col = 0; col < width; col++){
                switch (line[col]) {
                case 'J':
                    listJewels.push_back(Position(col,row));
                    break;
                case 'M':
                    mPos.set_xy(col,row);
                    break;
                case 'G':
                    listGoals.push_back(Position(col,row));
                    break;
                default:
                    break;
                }
                this->map[row][col] = line[col];
            }
        }

        // Print initial map
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                cout << map[y][x] << " ";
            }
            cout << endl;
        }

        // Print initial positions
        cout << "Man pos: ";
        mPos.print();
        cout << "Jewels pos" << endl;
        for(auto pos : listJewels)
            pos.print();
        cout << "Goals pos" << endl;
        for(auto pos : listGoals)
            pos.print();
    }
}

void Solver::detect_deadlocks(){
    /// REVISE THIS:
    /// 1. Find all corners (mark as deadlocks)
    /// 2. Check between corners if there exist a goal
    /// 2.1 If there exists a goal -> do nothing
    /// 2.2 If there does not exist a goal -> mark whole line as deadlock

    Position isDeadlock(0,0);
    int test = 0;
    for(int y = 1; y < height-1; y++){
        for(int x = 1; x < width-1; x++){
            if(map[y][x] != 'X' && map[y][x] != 'G'){
                // Find corners
                if(map[y+1][x] == 'X') isDeadlock.set_y(1);
                if(map[y-1][x] == 'X') isDeadlock.set_y(1);
                if(map[y][x+1] == 'X') isDeadlock.set_x(1);
                if(map[y][x-1] == 'X') isDeadlock.set_x(1);

                test = isDeadlock.get_x() + isDeadlock.get_y();
                if(test == 2) map[y][x] = 'x';
                // Reset
                isDeadlock.set_xy(0,0);
            }
        }
    }
    // Print map with deadlock prevention
    for(int y = 0; y < height; y++){
        for(int x = 0; x < width; x++){
            cout << map[y][x] << " ";
        }
        cout << endl;
    }

    /*for(int i = 1; i < 11; i++){
        open_list.push(node(i));
    }
    open_list.push(node(23));
    open_list.push(node(15));
    for(int i = 1; i < 13; i++){
        node t = open_list.top();
        open_list.pop();
        cout << t.heuristic << endl;
    }*/
}

void Solver::run(){

    open_list.push(root);

    while(!open_list.empty()){
        node curr_node = open_list.top();
        open_list.pop();

        if(is_solved()){
            solution = curr_node;
            break;
        }

        // Perform wavefront from each diamond (look at man's dist)

        // Add 4 nodes, one in each move direction
        add_node(curr_node,LEFT);
        add_node(curr_node,UP);
        add_node(curr_node,RIGHT);
        add_node(curr_node,DOWN);

        // Push to explored list
        closed_list.push_back(curr_node);
    }

}


void Solver::add_node(node curr_node, MOVEDIR dir){
    if(validate_move(curr_node,dir)){
        //If new node is valid

    }
}

bool Solver::validate_move(node curr_node, MOVEDIR dir){

}
