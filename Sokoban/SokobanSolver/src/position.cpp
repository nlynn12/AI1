#include "position.hpp"

using namespace std;

bool Position::operator ==(const Position &rhs) const{
    return x == rhs.x && y == rhs.y;
}

bool Position::operator !=(const Position &rhs) const{
    return x != rhs.x || y != rhs.y;
}

bool Position::operator <(const Position &rhs) const{
    return tie(x, y) < tie(rhs.x, rhs.y);
}

Position::Position(int x, int y){
    set_xy(x, y);
}

int Position::get_x() const{
    return x;
}

int Position::get_y() const{
    return y;
}

void Position::set_x(int x){
    this->x = x;
}

void Position::set_y(int y){
    this->y = y;
}

void Position::set_xy(int x, int y){
    set_x(x);
    set_y(y);
}

float Position::get_distance(Position pos){
    return sqrt(pow(x - pos.get_x(), 2) + pow(y - pos.get_y(), 2));
}

void Position::print(){
    cout << "(" << (int)x + 1 << ", " << (int)y + 1 << ")" << endl;
}
