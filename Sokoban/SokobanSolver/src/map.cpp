#include "map.hpp"

using namespace std;



unsigned int solution_step = 0;
//Init static variables
std::vector<std::vector<map::symbol> > map::layout;
std::vector<position> map::goal_positions;
map::wavefront_t map::distance_goals;
map::wavefront_t map::distance_diamonds;

uint8_t map::width           = 0,
        map::height          = 0,
        map::num_of_diamonds = 0;

map::map(string map_file_path){
    parse_map_file(map_file_path);
}

position map::get_man_position() const{
    return man_position;
}

std::vector<position> map::get_diamond_positions() const{
    return diamond_positions;
}

std::vector<position> map::get_goal_positions() const{
    return goal_positions;
}

void map::move_man(position &new_pos){
    //Update the mans position
    man_position = new_pos;
}

void map::push_diamond(position &current_pos, position &new_pos){
    diamond_positions_itr = find(diamond_positions.begin(), diamond_positions.end(), current_pos);

    //Did we find the diamond at that position
    if (diamond_positions_itr != diamond_positions.end()){
        //Update the diamond
        *diamond_positions_itr = new_pos;

        //Sort the diamonds.
        sort(diamond_positions.begin(), diamond_positions.end());

        //Update the mans postion
        move_man(current_pos);
    }else{
        cerr << "Diamond not found." << endl;
    }
}


map::symbol map::get_symbol(position &pos){
    symbol symbol_ = get_symbol_on_layout(pos);

    if(man_position == pos){
        if(symbol_ == FREE)
            symbol_ = MAN;
        else if(symbol_ == GOAL)
            symbol_ = MAN_GOAL;
    }else{
        diamond_positions_itr = find(diamond_positions.begin(), diamond_positions.end(), pos);

        if (diamond_positions_itr != diamond_positions.end()){
            if(symbol_ == FREE)
                symbol_ = DIAMOND;
            else if(symbol_ == GOAL)
                symbol_ = DIAMOND_GOAL;
        }
    }
    return symbol_;
}


map::symbol map::get_symbol_on_layout(position &pos){
    return pos.get_y() < height && pos.get_x() < width ? layout[pos.get_y()][pos.get_x()] : UNKNOWN;
}


void map::set_symbol(position &pos, symbol new_symbol){
    layout[pos.get_y()][pos.get_x()] = new_symbol;
}


bool map::solved(){
    bool is_solved = true;

    for (uint8_t diamond = 0; diamond < num_of_diamonds; ++diamond){
        if(get_symbol(diamond_positions[diamond]) != DIAMOND_GOAL){
            is_solved = false;
            break; // No need for checking the other diamonds
        }
    }
    return is_solved;
}

uint8_t map::get_distance_goal(position &current_pos){
    return distance_goals[current_pos.get_y()][current_pos.get_x()];
}


uint8_t map::get_distance_diamond(position &current_pos){
    return distance_diamonds[current_pos.get_y()][current_pos.get_x()];
}


void map::clear_wavefront(wavefront_t &wavefront){
    for (uint8_t y = 0; y < height; ++y){
        for (uint8_t x = 0; x < width; ++x){
            if(wavefront[y][x] != 1)
                wavefront[y][x] = 0;
        }
    }
}


void map::init_wavefront(wavefront_t &wavefront){
    //First resize the wavefront to fit the dimension of the sokoban map.
    wavefront.resize(height);

    //Then add ones were there is a wall or it is outside
    for (uint8_t y = 0; y < height; ++y){
        for (uint8_t x = 0; x < width; ++x){
            wavefront[y].resize(width);
            if(layout[y][x] == WALL || layout[y][x] == OUTSIDE){
                wavefront[y][x] = 1;
            }else{
                wavefront[y][x] = 0;
            }
        }
    }
}


void map::calc_distance_diamonds(){
    clear_wavefront(distance_diamonds);
    build_wavefront(distance_diamonds, diamond_positions);
}


void map::build_wavefront(wavefront_t &wavefront, positions_t &from_positions){
    queue<position> wavefront_queue;
    position current_pos,
             temp_pos;


    for(auto &postion : from_positions){
        wavefront_queue.push(postion);
        wavefront[postion.get_y()][postion.get_x()] = 2;
    }

    while (!wavefront_queue.empty()){
        current_pos = wavefront_queue.front();

        // Check arround the pixel
        for (uint8_t j = 0; j < 4; j++){
            switch (j){
            case 0:
                temp_pos.set_xy(current_pos.get_x(), current_pos.get_y() - 1);
                break;
            case 1:
                temp_pos.set_xy(current_pos.get_x(), current_pos.get_y() + 1);
                break;
            case 2:
                temp_pos.set_xy(current_pos.get_x() - 1, current_pos.get_y());
                break;
            case 3:
                temp_pos.set_xy(current_pos.get_x() + 1, current_pos.get_y());
                break;
            }

            // Make sure it not will go off the frame.
            if (temp_pos.get_x() <= width - 1 && temp_pos.get_y() <= height - 1 && temp_pos.get_x() >= 0 && temp_pos.get_y() >= 0){
                //Is that pos free.
                if (wavefront[temp_pos.get_y()][temp_pos.get_x()] == 0){
                    wavefront[temp_pos.get_y()][temp_pos.get_x()] = wavefront[current_pos.get_y()][current_pos.get_x()] + 1; // Then increment the wavevalue and insert it.
                    wavefront_queue.push(temp_pos); // And push that pos for further checking.
                }
            }
        }
        wavefront_queue.pop();
    }
}


void map::parse_map_file(string map_file_path){
    ifstream map_file(map_file_path);

    if (map_file.is_open()){
        get_map_info(map_file);
        build_layout(map_file);
        remove_man_and_diamonds();

        //Make the wavefronts used for distances
        init_wavefront(distance_goals);
        init_wavefront(distance_diamonds);
        build_wavefront(distance_goals, goal_positions);
        build_wavefront(distance_diamonds, diamond_positions);

        map_file.close();
    }else{
        cerr << "Unable to open the map file for reading." << endl;
    }
}

void map::get_map_info(std::ifstream &map_file){
    enum parse_state_t { WIDTH, HEIGHT, DIAMONDS, FINISHED };
    parse_state_t parse_state = WIDTH;

    string temp,
           line;

    getline(map_file, line);
    stringstream ss(line);

    //Go through each number and split for each space
    while (getline(ss, temp, ' ') && parse_state != FINISHED)
    {
        switch (parse_state)
        {
        case WIDTH:
            width = stoi(temp);
            parse_state = HEIGHT;
            break;
        case HEIGHT:
            height = stoi(temp);
            parse_state = DIAMONDS;
            break;
        case DIAMONDS:
            num_of_diamonds = stoi(temp);
            parse_state = FINISHED;
            break;
        default:
            break;
        }
    }
}

/**
 * @brief Builds the sokoban map from the file. Converts all symbols to an enum
 *        type, for easier debugging. Also finds the position of the man and diamonds.
 * @param map_file
 */
void map::build_layout(ifstream &map_file)
{
    string line;

    uint8_t x       = 0,
            y       = 0,
            diamond = 0,
            goal    = 0;

    //First resize the map layout to fit the dimension of the sokoban map.
    layout.resize(height);
    for (uint8_t y = 0; y < height; ++y)
    {
        layout[y].resize(width);
    }

    //Resize the vector to fit the number of diamonds
    diamond_positions.resize(num_of_diamonds);

    //Resize the vector to fit the number of goals
    goal_positions.resize(num_of_diamonds);

    //Go throug every line
    while (getline(map_file, line))
    {
        //Go throug every character in the line
        for (char& c : line)
        {
            if(y < height && x < width)
            {
                switch (c) {
                    case 'X':
                         layout[y][x] = WALL;
                         break;
                     case 'J':
                         layout[y][x] = DIAMOND;

                         if(diamond < num_of_diamonds)
                         {
                             diamond_positions[diamond].set_xy(x, y);
                             diamond++;
                         }
                         else
                         {
                             cerr << "Number of diamonds is out of scope." << endl;
                         }
                         break;
                     case 'G':
                         layout[y][x] = GOAL;

                         if(goal < num_of_diamonds)
                         {
                             goal_positions[goal].set_xy(x, y);
                             goal++;
                         }
                         else
                         {
                             cerr << "Number of goals is out of scope." << endl;
                         }
                         break;
                     case '.':
                         layout[y][x] = FREE;
                         break;
                     case 'M':
                         layout[y][x] = MAN;
                         man_position.set_xy(x, y);
                         break;
                     case ' ':
                         layout[y][x] = OUTSIDE;
                         break;
                     case 'D':
                         layout[y][x] = DEADLOCK;
                         break;
                     default:
                         layout[y][x] = UNKNOWN;
                         cerr << "There exists an unknown character in the map file look for a (-)." << endl;
                         break;
                 }
                 x++;
             }
             else
             {
                 cerr << "Dimensions out of scope." << endl;
                 return;
             }
         }
         y++;
         x = 0;
    }
}

/**
 * @brief This function remove man and diamonds from the layout
 */
void map::remove_man_and_diamonds()//hvad gør den? ændre bogstaverne i forhold til hvor robotten står? hvornår add/removed man mann/diament??
{
    //Pick the right symbol for when the man is removed
    if(get_symbol_on_layout(man_position) == MAN_GOAL)
    {
        set_symbol(man_position, GOAL);
    }
    else if(get_symbol_on_layout(man_position) == MAN_DEADLOCK)
    {
        set_symbol(man_position, DEADLOCK);
    }
    else
    {
        set_symbol(man_position, FREE);
    }

    for (uint8_t diamond = 0; diamond < num_of_diamonds; ++diamond)
    {
        //Pick the right symbol for when the diamond is removed
        if(get_symbol_on_layout(diamond_positions[diamond]) == DIAMOND_GOAL)
        {
            set_symbol(diamond_positions[diamond], GOAL);
        }
        else
        {
            set_symbol(diamond_positions[diamond], FREE);
        }
    }
}


/**
 * @brief This function adds man and diamonds to the layout
 */
void map::add_man_and_diamonds()
{
    //Pick the right symbol for the man
    if(get_symbol_on_layout(man_position) == GOAL)
    {
        set_symbol(man_position, MAN_GOAL);
    }
    else if(get_symbol_on_layout(man_position) == DEADLOCK)
    {
        set_symbol(man_position, MAN_DEADLOCK);
    }
    else
    {
        set_symbol(man_position, MAN);
    }

    //Set all diamonds position as free
    for (uint8_t diamond = 0; diamond < num_of_diamonds; ++diamond)
    {
        //Pick the right symbol for the diamond
        if(get_symbol_on_layout(diamond_positions[diamond]) == GOAL)
        {
            set_symbol(diamond_positions[diamond], DIAMOND_GOAL);
        }
        else
        {
            set_symbol(diamond_positions[diamond], DIAMOND);
        }
    }
}

/**
 * @brief Prints sokoban map and info.
 */
void map::print()
{
    add_man_and_diamonds();

    cout << "----------Sokoban map----------" << endl;
    cout << "Width: " << (int)width << " Height:" << (int)height << " #Diamond: " << (int)num_of_diamonds << endl;
    for (uint8_t y = 0; y < height; ++y)
    {
        for (uint8_t x = 0; x < width; ++x)
        {
            cout << (char)layout[y][x];
        }
        cout << endl;
    }

    cout << "Man position: ";
    man_position.print();

    for (uint8_t diamond = 0; diamond < num_of_diamonds; ++diamond)
    {
        cout << "Diamond " << (int)diamond + 1 << " position: ";
        diamond_positions[diamond].print();
    }

    cout << "------------------------------" << endl;

    remove_man_and_diamonds();
}
