/*
 *      University of Southern Denmark
 *      Introduction to Artificial Intelligence
 *
 *      Project:        Sokoban Solver
 *      Module name:    main.cpp
 *
 *      Description:    Starts the Sokoban Solver
 *
 *      Author(s):      Juan Luis Hortelano Villanueva
 *                      Nicolai Anton Lynnerup
 *
 *      ------------------------------------------------------
 *      Change Log:
 *
 *      Date            ID              Change
 *      YYMMDD
 *      151118          NAL             Module Created
 *
 *      ------------------------------------------------------
 */

/***************************** Include files *******************************/
#include <iostream>
#include "solver.hpp"
#include "map.hpp"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

using namespace std;

int main(){

    cout << "Sokoban Solver start." << endl;

    Solver solver_("maps/level4.txt");

    cout << "Sokoban Solver finished." << endl;
    return 0;
}
