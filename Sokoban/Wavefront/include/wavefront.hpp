/*
 *      University of Southern Denmark
 *      Introduction to Artificial Intelligence
 *
 *      Project:        Sokoban Solver
 *      Module name:    wavefront.hpp
 *
 *      Description:    Implements the Wavefront
 *
 *      Author(s):      Juan Luis Hortelano Villanueva
 *                      Nicolai Anton Lynnerup
 *
 *      ------------------------------------------------------
 *      Change Log:
 *
 *      Date            ID              Change
 *      YYMMDD
 *      151116          NAL             Module Created
 *
 *      ------------------------------------------------------
 */

/***************************** Include files *******************************/
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <queue>

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

#ifndef WAVEFRONT_H
#define WAVEFRONT_H

using namespace std;

class Wavefront{
public:
    typedef vector<vector<uint8_t>> wavefront_t;
    typedef pair<int,int> position_t;
    typedef vector<pair<int,int>> position_holder_t;
    enum print_type{
        CHAR,
        NUMERIC
    };
    wavefront_t map, wavefront;
    Wavefront(std::string);
    void execute(wavefront_t&,vector<pair<int,int>>);
    void print(wavefront_t, print_type);
private:
    void load_map(string);
    int width, height, n_jewels, n_goals;
};



#endif //WAVEFRONT_H
