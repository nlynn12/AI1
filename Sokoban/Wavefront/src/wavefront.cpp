#include "wavefront.hpp"

Wavefront::Wavefront(std::string path){
    load_map(path);

    wavefront.resize(height);
    for(int y = 0; y < height; y++){
        wavefront[y].resize(width);
        for(int x = 0; x < width; x++){
            switch(map[y][x]){
            case 'X':
            case ' ':
                wavefront[y][x] = 1;
                break;
            case 'G':
                n_goals++;
                wavefront[y][x] = 0;
            case 'J':
            case 'M':
            case '.':
                wavefront[y][x] = 0;
                break;
            }
        }
    }
    print(wavefront,NUMERIC);
}

void Wavefront::load_map(std::string path){
    ifstream infile(path);
    string line;

    if(!infile.is_open()){
        cout << "Unable to open: \"" + path + "\"" << endl;
        exit(-1);
    } else {
        cout << "Map: \"" + path + "\" read "<< endl;

        // Load in Map info
        getline(infile, line);
        stringstream ss;
        ss << line;
        ss >> this->width >> this->height >> this->n_jewels;
        cout << width << " " << height << " " << n_jewels << endl;

        // Load in original Map
        map.resize(this->height,vector<uint8_t>(this->width));
        for(int row = 0; row < this->height; row++){
            getline(infile, line);
            for(int col = 0; col < this->width; col++){
                this->map[row][col] = (uint8_t)line[col];
            }
        }
        print(map,CHAR);
    }
}

void Wavefront::print(wavefront_t  map, print_type type = CHAR){
    cout << endl << "----------- Map -----------" << endl;
    for(int i = 0; i < this->height; i++){
        cout << "\t";
        for(int j = 0; j < this->width; j++){
            if(type == CHAR)
                cout << (char)map[i][j] << " ";
            else
                cout << setw(2) << (int)map[i][j] << " ";
        }
        cout << endl;
    }
    cout << "---------------------------" << endl;
}


void Wavefront::execute(wavefront_t &wavefront, vector<pair<int,int>> from_positions){
    queue<position_t> wavefront_queue;
    position_t curr_pos, temp_pos;

    for(auto position : from_positions){
        wavefront_queue.push(position);
        wavefront[position.second][position.first] = 2;
    }

    while(!wavefront_queue.empty()){
        curr_pos = wavefront_queue.front();

        for(int i = 0; i < 4; i++){
            switch (i) {
            case 0:
                temp_pos = make_pair(curr_pos.first,curr_pos.second-1);
                break;
            case 1:
                temp_pos = make_pair(curr_pos.first,curr_pos.second+1);
                break;
            case 2:
                temp_pos = make_pair(curr_pos.first-1,curr_pos.second);
                break;
            case 3:
                temp_pos = make_pair(curr_pos.first+1,curr_pos.second);
                break;
            default:
                break;
            }

            if(temp_pos.first < width && temp_pos.second < height && temp_pos.first >= 0 && temp_pos.second >= 0){
                if(wavefront[temp_pos.second][temp_pos.first]==0){
                    wavefront[temp_pos.second][temp_pos.first] = wavefront[curr_pos.second][curr_pos.first] + 1;
                    wavefront_queue.push(temp_pos);
                }
            }
        }
        wavefront_queue.pop();
    }
    print(wavefront,NUMERIC);
}
