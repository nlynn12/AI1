#include <iostream>
#include <utility>
#include "wavefront.hpp"

using namespace std;

int main(){
    Wavefront test("maps/level9.txt");

    vector<pair<int,int>> goals;
    goals.push_back(make_pair(3,9));
    //goals.push_back(make_pair(2,3));

    test.execute(test.wavefront,goals);
    return 0;
}
