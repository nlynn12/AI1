\chapter{Design and Implementation of the Path Finding Algorithm}
For deciding on an implementation strategy several solutions were considered. Each of these solutions looked at different factors of the Sokoban game. For instance minimizing the number of diamond moves or minimizing the number of man moves. This leads to the observation that two factors in this game is to be controlled, namely the man and the diamonds. Everything else in the game is static. However only the diamonds' positions are the basis for evaluating the original game, the man is only the tool for moving them. A solution is created at the point where every diamond is at their respective goals in the map.

After several implementation trials a general path finding algorithm were found and hereby utilized in the final version of the Sokoban Solver, namely the A* algorithm. And for this the solution sought is to (1) find the optimal path for the diamonds' current positions to their respective goals and (2) find a path from the man's current position to the nearest diamond. The most important requirement is that it produces the shortest path possible, even in the expense of computition time, as it directly affects the robot's performance. The most important features that the solver needs are: Map awareness and path finding described in detail in the next sections.

\section{Map Awareness}
The first thing the solver has to do is to be able to read the txt file which contains the map and from this extract the positions for each character of the game. In order to achieve this, a matrix of characters was used to store the map and a class for holding interest points' positions were created. This is convenient because a the relation between $(x,y)$ coordinates and an actual matrix position is direct.

\subsection{Deadlock Detection}
In this solver it is decided to detect deadlocks in the map, i.e. positions where the diamond can only be moved to and not moved from. A deadlock is a point in the map in which, if a jewel falls in, the level isn't solvable anymore. Deadlocks usually appear in corners, however if a goal is not placed along a straight wall (i.e. between two corners) then the whole free path next to the wall can efficiently be marked as being a deadlock for diamonds. This should significantly reduce the number of nodes created by the solver.

There is several ways to avoid falling in a deadlock, the one chosen for this solver is to detect the deadlocks in the map and place fake walls on those spots as an initial step to the path finding algorithm. It is important that these new walls are differentiated from the originals as the robot has to be able to move in deadlock positions. This means that the deadlock detection is only used when checking if a diamond push is valid, not when checking if a man move is valid.


\section{Path Finding using Informed Search (A*)}
For finding an optimal path inwhich the man can move the diamonds to their respective goals it is needed to store each state of the game in a tree data structure. It is in this tree data structure that the A* will search for a solution. Each state in the game is directly corresponding to a node in the tree. A state of the game is unique in the sense of the man's and the diamonds' positions. Everything else in the game, i.e. walls, goals and freespace is static and is therefore not needed to be stored. This way a significant amount of memory is saved. Beside storing the positions the nodes needs to contain a cost and a heuristic for the open list and a unique hashkey for the closed list in the A* path finding algorithm. The diamond pushes are directed with the heuristics and cost and is hereby goal oriented. However cases do occur where a diamond is needed to be pushed away from the goal in order to solve the map. This non-goal oriented push is done when everything else fails, i.e. the diamonds can, in their current state, not be moved closer to the goal.

If a map contains 4 diamonds, as in the case of the 2015 competition map, then each node contains a total of 5 positions yielding $10 \cdot 8\text{bits} = 80\text{bits}$, a cost and a heuristic yielding $2 \cdot 16\text{bits} = 32\text{bits}$, and lastly a hashkey created from the 5 positions yielding a 5 character string, i.e. 40bits. So memory wise each node uses 152bits.\\

\noindent Algorithm \ref{alg:sokoban_solver} below describes the main part of the sokoban solver.

\begin{algorithm}
\caption{Main Algorithm the Sokoban Solver}\label{alg:sokoban_solver}
\begin{algorithmic}[1]
\State \textbf{set} \textit{root.map} to \textit{map}
\State \textbf{set} \textit{root.parent} to \textit{null}
\State \textbf{push} \textit{root} to \textit{openlist}
\While{\textit{openlist} not empty}
  \State \textbf{set} \textit{currentnode} to first node in \textit{openlist}
  \State \textbf{pop} node from \textit{openlist}
  \If{\textit{currentnode.map} is solution}
    \State \textbf{return} solution
  \EndIf
  \For {\textbf{each} \textit{node} in \textit{currentnode.neighborhood}}
    \If{\textit{node} is valid}
      \State \textbf{compute} \textit{node.hashkey}
      \If{\textit{node} is not in \textit{closedlist}}
	\State \textbf{compute} \textit{node.cost}
	\State \textbf{compute} \textit{node.heuristic}
	\State \textbf{push} \textit{node} to \textit{openlist}
      \EndIf
    \EndIf
  \EndFor
\EndWhile
\end{algorithmic}
\end{algorithm}

\subsection{Validating a New Node}
In algorithm \ref{alg:sokoban_solver} line 11 should be noticed. Here each neighboring node is checked to be valid and if it is then it will be added to tree. The validation works by checking what character is located in the orignal map without the man and diamonds in it. This is then compared to the current locations of the diamonds and the man. If the character is a wall then the validation fails. If the character is a deadlock, a goal or freespace the move validation succeeds. If the character is a diamond then the validation of the move succeeds and is marked as a diamond push. Then it needs to validate the diamond push. This is done, again by checking what character is in the map at the diamond's new position. If the character is freespace or a goal then the validation of the push succeeds. This results in the positions of the diamond and man to be updated. It furthermore results in the neighboring node to be added to the tree. If the character is otherwise, i.e.; wall, deadlock or diamond then the push fails and the node is not added to the tree.


\subsection{Determining Cost and Heuristics for the Graph Search Algorithm}
As the A* is a directed graph search algorithm it needs a measure on how to choose the next step, i.e. the next node in the tree. For this a cost and a heuristic is computed for each node.

\subsubsection*{Computing the Cost for each Node}
The cost is computed by taking the parent's cost and incrementing this by one. The roots, i.e. the first node's cost, equals to 1. This means that each level in the tree corresponds to the cost for the nodes at that level. E.g. on level 3 every node has the cost 3.

\subsubsection*{Computing the Heuristic for each Node}
The heuristics is based on two wavefronts generated from the map. The wavefront is chosen as it always gives the shortest distance from a start to a goal. The first part of the heuristics is the diamonds distance to the nearest goals and this is based on a wavefront initiated from each of the goals in the map. As the goals are static in the game, this wavefront is only generated once and then stored globally inside the Sokoban solver class. From here it can be used as a lookup table of numbers describing the distance from the current position to the nearest goal. The second part of the heuristics is the man's distance from himself to the nearest diamond. As the diamonds continuously are moved around in the map, this wavefront needs to be generated once for each state of the map. So each time a node is created a wavefront is initiated at the diamonds and expands through the map to the man. Then it is possible to find the shortest path from the man to the nearest diamond.

\subsubsection*{Usage of Cost and Heuristics}
The cost and heuristic are then simply added together in an operator overload which is used in the open list of the A* path finding algorithm. The open list is implemented as a min heap as this is always sorted with the node which has the smallest cost and heuristics (i.e. distance) in the top. This means that the data structure gives a constant lookup time when a new node needs to be popped from the open list.

\subsection{Implementation of the Closed List}
As described above the closed list is implemented as a hashtable (called \lstinline|unorderedmap| in C++). This is done as it is needed upon creation of new nodes to check if the new node has already been explored, which again is done by looking in the closed list as shown in algorithm \ref{alg:sokoban_solver}. By utilizing a hashtable which gives a constant lookup time the time consumption should drastically decrease. The increase in speed is traded for the extra memory consumption of the hashkey.


\chapter{Tests on the Sokoban Solver}
The Sokoban Solver has been tested for functionality on a range of different maps prior to recieving the 2015 competition map, however due to time issues memory tests and time tests has not been developed. One of the maps used for functionality test shows the benefit of using deadlock detection and the wavefront algorithm for the heuristics, see figure \ref{fig:test_map_comp}.

\begin{figure}[H]
 \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/Selection_050}
        \caption{Test map illustrated with the QML sokoban project available from \url{http://qt-apps.org/content/show.php/QML+Sokoban?content=134799}}
        \label{fig:map_test}
    \end{subfigure}
     
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=0.8\textwidth]{img/map_deadlock}
        \caption{The map shown with all the corners marked as deadlocks}
        \label{fig:map_deadlock}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.55\textwidth}
        \includegraphics[width=\textwidth]{img/map_wavefront}
        \caption{The wavefront algorithm's output. It is seen that this estimate of the true distance is much better than using euclidean or manhattan distance estimates.}
        \label{fig:map_wavefront}
    \end{subfigure}
    \caption{The figures illustrates a map which shows the benefits of using both deadlock detection and the wavefront algorithm for heuristics.}
    \label{fig:test_map_comp}
\end{figure}

%\chapter{Discussion and Conclusion}