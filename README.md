AI1 - Introduction to Artificial Intelligence
====
University of Southern Denmark

Course Description
====

Knowledge
---------
* familiarity with the classical, behaviour-based and embodied
  approaches to AI
* state-space representational techniques, search algorithms that
  employ them, and simple knowledge-based action planning techniques
* heuristics: what heuristics are, and how to use heuristics in search.

Skills
------
* able to design and implement a solution to non-trivial
  search-based problems
* able to interface AI software and robotic components to complete a
  real-world task
* able to devise simple but reliable robotic agents to support AI
  reasoning software

Competences
-----------
* able to recognise problems for which a state-based search AI
  approach is viable or profitable
* able to choose tools and encoding methods for representing a
  suitable AI problem in a state-based formulation
* able to select appropriate search and/or planning algorithms for a
  knowledge-based problem
